Project that queries the price of various Crypto currencies

# Project Set up
> pip install virtualenv
> git clone git@bitbucket.org:andrewmcglynn86/cryptochecker.git
> virtualenv --no-site-packages CryptoChecker
> cd CryptoChecker
> source bin/activate

> pip install requests

Leave the virtualenv
> deactivate


Set up virtualenv in PyCharm
* Open Preferences
* Project: CryptoChecker
* Project Interpreter
* Click the + button
* Click Create VirtualEnv
* Search for and add 'requests'