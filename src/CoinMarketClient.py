import requests, json
from Coin import Coin

class CoinMarketClient:
    def get_available_coin_ids(self):
        available_coin_ids = list()
        coins_response = requests.get('https://api.coinmarketcap.com/v1/ticker').text

        coins = json.loads(coins_response)
        for coin in coins:
            coinObj = Coin(coin)
            available_coin_ids.append(coinObj.id)

        return available_coin_ids

    def get_coin_details(self, coin_id):
        coins_response = requests.get('https://api.coinmarketcap.com/v1/ticker/' + coin_id).text
        return Coin(json.loads(coins_response)[0])
