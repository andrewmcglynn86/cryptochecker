class Coin:
    def __init__(self, coinDict):
        for key in coinDict:
            setattr(self, key, coinDict[key])

    def get_name(self):
        return self.name

    def get_id(self):
        return self.id

    def get_symbol(self):
        return self.symbol

    def get_rank(self):
        return self.rank

    def get_price_usd(self):
        return self.price_usd

    def get_price_btc(self):
        return self.price_btc

    def get_24h_volume_usd(self):
        return getattr(self, "24h_volume_usd")

    def get_market_cap_usd(self):
        return self.market_cap_usd

    def get_available_supply(self):
        return self.available_supply

    def get_total_supply(self):
        return self.total_supply

    def get_percent_change_1h(self):
        return self.percent_change_1h

    def get_percent_change_24h(self):
        return self.percent_change_24h

    def get_percent_change_7d(self):
        return self.percent_change_7d

    def get_last_updated(self):
        return self.last_updated
