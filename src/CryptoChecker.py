from CoinMarketClient import CoinMarketClient


def print_coin(coin):
    print "Name: " + coin.get_name()
    print "Symbol: " + coin.get_symbol()
    print "ID: " + coin.get_id()
    print "Price USD: " + coin.get_price_usd()
    print "Price BTC: " + coin.get_price_btc()
    print "Available Supply: " + coin.get_available_supply()
    print "Percent Change 1H: " + coin.get_percent_change_1h()
    print "Percent Change 24H: " + coin.get_percent_change_24h()
    print "Percent Change 7 Days: " + coin.get_percent_change_7d()
    print "Total Supply: " + coin.get_total_supply()
    print "24H Volume USD: " + coin.get_24h_volume_usd()
    print "Market Cap (USD): " + coin.get_market_cap_usd()
    print "Rank: " + coin.get_rank()
    print "Last Updated: " + coin.get_last_updated()

client = CoinMarketClient()
coin = client.get_coin_details('bitcoin')
print_coin(coin)
